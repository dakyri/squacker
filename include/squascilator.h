#pragma once

#include <string>

#include "audio_util.h"
#include "tinyxml2.h"
#include "BoundedTableOscilator.h"

namespace tinyxml2 {
	class XMLElement;
}

class Squascilator {
public:
	Squascilator();

	status_t setFormat(uint nChan, uint sampleLenBits, ulong sampleRateHz);
	status_t play(uint32_t nframes, byte_t *data);

	long loadOsc(tinyxml2::XMLElement *);

	BoundedTableOscilator osc;

	float gain;
	ulong startPoint;
	ulong endPoint;
};
