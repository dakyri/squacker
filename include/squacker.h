#pragma once

#include <string>
#include <vector>

#include "audio_util.h"
#include "squascilator.h"
#include "tinyxml2.h"


namespace tinyxml2 {
	class XMLElement;
}

class Squacker {
public:
	Squacker();

	status_t setFormat(uint nChan, uint sampleLenBits, ulong sampleRateHz);
	status_t play(uint32_t nframes, byte_t *data);

	status_t loadSquack(tinyxml2::XMLElement *);
	status_t loadBank(tinyxml2::XMLElement *);
	status_t loadConfigFile(const std::string path);

	float gain;
	std::vector<Squascilator> oscilators;

};