#include "squascilator.h"
#include "SampleFile.h"

Squascilator::Squascilator() {

}


status_t
Squascilator::setFormat(uint nChan, uint sampleLenBits, ulong sampleRateHz)
{
	return OKIDOKI;
}

status_t
Squascilator::play(uint32_t nframes, byte_t *data)
{
	return OKIDOKI;
}


/*!
 * load xml data for this oscilators value in a preset
 *  <osc sample="some file" start="offset" end="offset" level="gain"/>
 */
long
Squascilator::loadOsc(tinyxml2::XMLElement *preset)
{
	const std::string el_name = preset->Name();
	if (el_name != "osc") {
		return -1;
	}
	SampleFile	file;
	return 0;
}