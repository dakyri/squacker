#ifdef _WIN32

#include <Audioclient.h>
#include <Audiopolicy.h>
#include <mmdeviceapi.h>

#include "../include/squacker.h"
#include "squack-win.h"

//-----------------------------------------------------------
// Play an audio stream on the default audio rendering
// device. The PlayAudioStream function allocates a shared
// buffer big enough to hold one second of PCM audio data.
// The function uses this buffer to stream data to the
// rendering device. The inner loop runs every 1/2 second.
//-----------------------------------------------------------

// REFERENCE_TIME time units per second and per millisecond
#define REFTIMES_PER_SEC  10000000
#define REFTIMES_PER_MILLISEC  10000

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioRenderClient = __uuidof(IAudioRenderClient);

HRESULT PlayAudioStream(Squacker *squack)
{
	HRESULT hr;
	REFERENCE_TIME hnsRequestedDuration = REFTIMES_PER_SEC;
	REFERENCE_TIME hnsActualDuration;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioClient *pAudioClient = NULL;
	IAudioRenderClient *pRenderClient = NULL;
	WAVEFORMATEX *pwfx = NULL;
	UINT32 bufferFrameCount;
	UINT32 numFramesAvailable;
	UINT32 numFramesPadding;
	BYTE *pData;
	DWORD flags = 0;

	hr = CoCreateInstance(
		CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator,
		(void**)&pEnumerator);
	if (hr < 0) { goto Exit; }

	hr = pEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pDevice);
	if (hr < 0) { goto Exit; }

	hr = pDevice->Activate(IID_IAudioClient, CLSCTX_ALL, NULL, (void**)&pAudioClient);
	if (hr < 0) { goto Exit; }

	hr = pAudioClient->GetMixFormat(&pwfx);
	if (hr < 0) { goto Exit; }

	hr = pAudioClient->Initialize(
		AUDCLNT_SHAREMODE_SHARED,
		0,
		hnsRequestedDuration,
		0,
		pwfx,
		NULL);
	if (hr < 0) { goto Exit; }

	// Tell the audio source which format to use.
	int nChan = pwfx->nChannels;
	int sampleLenBits = pwfx->wBitsPerSample;
	long sampleRateHz = pwfx->nSamplesPerSec;
	// pwfx->nBlockAlign,  pwfx->nAvgBytesPerSec,  pwfx->wFormatTag
	if (pwfx->cbSize >= 22) {
		WAVEFORMATEXTENSIBLE *pwfxxx = reinterpret_cast<WAVEFORMATEXTENSIBLE *>(pwfx);
	}
	status_t status = squack->setFormat(nChan, sampleLenBits, sampleRateHz);
	if (status != OKIDOKI) { goto Exit; }

	// Get the actual size of the allocated buffer.
	hr = pAudioClient->GetBufferSize(&bufferFrameCount);
	if (hr < 0) { goto Exit; }

	hr = pAudioClient->GetService(
		IID_IAudioRenderClient,
		(void**)&pRenderClient);
	if (hr < 0) { goto Exit; }

	// Grab the entire buffer for the initial fill operation.
	hr = pRenderClient->GetBuffer(bufferFrameCount, &pData);
	if (hr < 0) { goto Exit; }

	// Load the initial data into the shared buffer.
	status = squack->play(bufferFrameCount, pData);
	if (status < 0) { goto Exit; }
	else if (status != OKIDOKI) { flags = AUDCLNT_BUFFERFLAGS_SILENT; }

	hr = pRenderClient->ReleaseBuffer(bufferFrameCount, flags);
	if (hr < 0) { goto Exit; }

	// Calculate the actual duration of the allocated buffer.
	hnsActualDuration = REFERENCE_TIME(double(REFTIMES_PER_SEC)* bufferFrameCount / pwfx->nSamplesPerSec);

	hr = pAudioClient->Start();  // Start playing.
	if (hr < 0) { goto Exit; }

	// Each loop fills about half of the shared buffer.
	while (flags != AUDCLNT_BUFFERFLAGS_SILENT) {
		// Sleep for half the buffer duration.
		Sleep((DWORD)(hnsActualDuration / REFTIMES_PER_MILLISEC / 2));

		// See how much buffer space is available.
		hr = pAudioClient->GetCurrentPadding(&numFramesPadding);
		if (hr < 0) { goto Exit; }

		numFramesAvailable = bufferFrameCount - numFramesPadding;

		// Grab all the available space in the shared buffer.
		hr = pRenderClient->GetBuffer(numFramesAvailable, &pData);
		if (hr < 0) { goto Exit; }

		// Get next 1/2-second of data from the audio source.
		status = squack->play(numFramesAvailable, pData);
		if (status < 0) { goto Exit; }
		else if (status != OKIDOKI) { flags = AUDCLNT_BUFFERFLAGS_SILENT; }

		hr = pRenderClient->ReleaseBuffer(numFramesAvailable, flags);
		if (hr < 0) { goto Exit; }
	}

	// Wait for last data in buffer to play before stopping.
	Sleep((DWORD)(hnsActualDuration / REFTIMES_PER_MILLISEC / 2));

	hr = pAudioClient->Stop();  // Stop playing.
	if (hr < 0) { goto Exit; }

Exit:
	CoTaskMemFree(pwfx);
	if (pEnumerator != NULL) { pEnumerator->Release(); pEnumerator = NULL; }
	if (pDevice != NULL) { pDevice->Release(); pDevice = NULL; }
	if (pAudioClient != NULL) { pAudioClient->Release(); pAudioClient = NULL; }
	if (pRenderClient != NULL) { pRenderClient->Release(); pRenderClient = NULL; }

	return hr;
}

int
main(int argc, char* argv[]) {
	Squacker squacker;
	squacker.loadConfigFile("presets/preset1.xml");
	PlayAudioStream(&squacker);
	return 0;
}

#endif