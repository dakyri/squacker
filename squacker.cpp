#include "squacker.h"

Squacker::Squacker()
{

}

status_t
Squacker::setFormat(uint nChan, uint sampleLenBits, ulong sampleRateHz)
{
	return OKIDOKI;
}

status_t
Squacker::play(uint32_t nframes, byte_t *data)
{
	for (auto & osc : oscilators) {
		status_t err = osc.play(nframes, data);
	}
	return OKIDOKI;
}

status_t
Squacker::loadConfigFile(const std::string path)
{
	tinyxml2::XMLDocument doc;
	FILE *xml = fopen(path.c_str(), "r");
	if (xml == nullptr) {
		return 0;
	}
	tinyxml2::XMLError err = doc.LoadFile(xml);
	if (err != tinyxml2::XML_SUCCESS) {
		fclose(xml);
		return -1;
	}
	tinyxml2::XMLElement* root = doc.RootElement();
	if (loadBank(root) != 0) {
		fclose(xml);
		return -1;
	}
	fclose(xml);
	return 0;
}

/*!
 * load xml data for this oscilators value in a preset
 *  <preset name="some name" gain="some float">
 *    <osc ...>
 *    ...
 *  </preset>
 */
status_t
Squacker::loadBank(tinyxml2::XMLElement *presets)
{
	const std::string el_name = presets->Name();
	if (el_name == "bank") {

	} else if (el_name == "squack") {
		loadSquack(presets);
	} else {
		return -1;
	}
	return 0;
}

status_t
Squacker::loadSquack(tinyxml2::XMLElement *presets)
{
	const std::string el_name = presets->Name();
	if (el_name == "squack") {
		return 0;
	}
	return -1;
}